#include <unistd.h>             /* fork, execve, read */
#include <sys/wait.h>           /* waitpid */
#include <stdio.h>
#include <stdlib.h>

#define TRUE 1
#define PROMPT "$ "
#define MAX_LEN 500
#define MAX_PARAMS 10

void type_prompt(int last_status){
  fprintf(stdout, "[%d]$ ", last_status);
}

void read_command(char **cmd, char ***parameters){
  size_t len = MAX_LEN;
  ssize_t real_len;
  static char *tmp = NULL;
  if(tmp){
    free(tmp);
  }
  tmp = (char *)malloc(sizeof(char) * MAX_LEN);
  real_len = getline(&tmp, &len, stdin);
  /* trim the \n */
  tmp[real_len -1] = '\0';

  /* find the spaces */

  /* pointer to the first character in tmp */
  char *last_token = tmp;

  /* malloc'd array of tmp parameters */
  /* these are pointers to pointers */
  static char **params = NULL;
  if(params){
    free(params);
  }
  params = (char **)malloc(sizeof(char *) * (MAX_PARAMS + 1));

  /* this is a pointer to the current position in params */
  char **cur_param = params;

  char *cur_tmp = tmp;

  int num_params = 0;
  
  /* while the value of tmp isn't null */
  while(*cur_tmp){
    /* check if the value of cur_tmp is a space */
    if(*cur_tmp == ' ' ){
      *cur_param = last_token;
      *cur_tmp = '\0';
      num_params++;
      if(num_params >= MAX_PARAMS){
        break;
      } else{
        cur_param++;
        last_token = cur_tmp + 1;
      }
    }
    cur_tmp++;
  }

  /* we always have one left at the end */
  *cur_param = last_token;
  *(cur_param + 1) = 0;
  
  /* this is dangerous if background jobs are enabled */
  *cmd = params[0];
  *parameters = &params[1];
  int count = 0;
  char **cur_params = params;
  cur_params++;
  while(*cur_params){
    cur_params++;
  }
}

/*    implemented: */
/* simple parsing of commands and parameters */

/*    desired: */
/* C-p, C-n, C-f, C-b, M-f, M-b, C-k, C-y */
/* redirection of input/output */
/* pipes */
/* background jobs */

int main(int argc, char **argv){
  char *command;
  char **parameters;
  char **cur_param;
  pid_t status;
  int last_status = 0;
  
  while(TRUE){                    /* repeat forever */
    type_prompt(last_status);     /* display prompt on the screen */
    read_command(&command, &parameters); /* read input from terminal */

    if(fork() != 0){            /* fork off child process */
      /* parent code */
      pid_t child_pid = waitpid(-1, &status, 0); /* wait for child to exit */
      last_status = WEXITSTATUS(status);
    }else{
      /* child code */
      int succ = execve(command, parameters, 0); /* execute command */
      /* this shouldn't ever happen */
      last_status = succ;
    }
  }
 return EXIT_SUCCESS;
}
